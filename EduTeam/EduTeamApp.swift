//
//  EduTeamApp.swift
//  EduTeam
//
//  Created by Ahmad Nur Alifullah on 29/09/21.
//

import SwiftUI

@main
struct EduTeamApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
